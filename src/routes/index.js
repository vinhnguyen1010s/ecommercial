"use strict";

const express = require("express");
const { apiKey, permissions } = require("../auth/checkAuth");

const router = express.Router();

router.get(``, (req, res, next) => {
  // const strCompress = "Hello Nodejs express";
  return res.status(200).json({
    message: "Welcome Nodejs",
    // metadata: strCompress.repeat(100000)
  });
});

// check apikey
router.use(apiKey);

// check permissions
router.use(permissions("0000"));

router.use(`/v1/api`, require("./access"));
router.use(`/v1/api/product`, require("./product"));

module.exports = router;
