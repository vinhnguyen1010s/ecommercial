"use strict";

const express = require("express");
const productController = require("../../controlers/product.controller");
const { asyncHandle } = require("../../helpers/asyncHandle");
const { authenticationV2 } = require("../../auth/authUtils");

const router = express.Router();

// authentication
router.use(authenticationV2);

// create product
router.post(``, asyncHandle(productController.createProduct));

module.exports = router;
