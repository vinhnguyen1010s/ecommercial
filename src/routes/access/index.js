"use strict";

const express = require("express");
const accessController = require("../../controlers/access.controller");
const { authenticationV2 } = require("../../auth/authUtils");
const { asyncHandle } = require("../../helpers/asyncHandle");

const router = express.Router();

// login
router.post(`/shop/login`, asyncHandle(accessController.login));

// signup
router.post(`/shop/signup`, asyncHandle(accessController.signUp));

// authentication
router.use(authenticationV2);

// logout
router.post(`/shop/logout`, asyncHandle(accessController.logout));

router.post(
  `/shop/refresh-token`,
  asyncHandle(accessController.handleRefreshToken)
);

module.exports = router;
