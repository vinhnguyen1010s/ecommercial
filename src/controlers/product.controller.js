"use strict";

const { SuccessResponse } = require("../code/success.response");
// const ProductService = require("../services/product.service");
const Productx2Service = require("../services/productx2.service");

class ProductController {
  createProduct = async (req, res, next) => {
    // v1
    // new SuccessResponse({
    //   message: "Create new product success.",
    //   metadata: await ProductService.createProduct(req.body.product_type, {
    //     ...req.body,
    //     product_shop: req.user.userId,
    //   }),
    // }).send(res);
    new SuccessResponse({
      message: "Create new product success.",
      metadata: await Productx2Service.createProduct(req.body.product_type, {
        ...req.body,
        product_shop: req.user.userId,
      }),
    }).send(res);
  };
}

module.exports = new ProductController();
