"use strict";
const mongoose = require("mongoose");

const connectString = `mongodb://127.0.0.1:27017/dbapp`;

mongoose
  .connect(connectString, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log("Database connection successful");
  })
  .catch((err) => {
    console.error("Database connection error:", err);
  });

// dev
if (1 === 1) {
  mongoose.set("debug", true);
  mongoose.set("debug", { color: true });
}

// module.exports = connectionDB;
module.exports = mongoose;
