"use strict";
const mongoose = require("mongoose");
const { countConnect } = require("../helpers/checkConnect");
const {
  db: { host, name, port },
} = require("../configs/config.mongodb");

const connectString = `mongodb://${host}:${port}/${name}`;
console.log(`${connectString}`);
class Database {
  constructor() {
    this.connect();
  }

  // connnect
  connect(type = "mongodb") {
    if (1 === 1) {
      mongoose.set("debug", true);
      mongoose.set("debug", { color: true });
    }

    mongoose
      .connect(connectString, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        maxPoolSize: 100,
      })
      .then(() => {
        console.log("Database connection successful PRO", countConnect());
      })
      .catch((err) => {
        console.error("Database connection error:", err);
      });
  }

  static getInstance() {
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }
}

const instanceMongodb = Database.getInstance();
module.exports = instanceMongodb;
