const express = require("express");
const morgan = require("morgan");
const helmet = require("helmet");
const compression = require("compression");
require("dotenv").config();
const { checkOverload } = require("./helpers/checkConnect");
const app = express();

// init middleware
app.use(morgan("dev"));
app.use(helmet());
app.use(compression()); // Dùng để tối ưu hóa sizre khi load data
// app.use(morgan("combined"));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
// init db
// require("./dbs/init.connect-mongodb.lv0");

require("./dbs/init.connect-mongodb");

// checkOverload();
// init routers
// app.get(`/`, (req, res, next) => {
//   // const strCompress = "Hello Nodejs express";
//   return res.status(200).json({
//     message: "Welcome Nodejs",
//     // metadata: strCompress.repeat(100000)
//   });
// });

app.use("/", require("./routes"));

// init handling error
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  const statusCode = error.status || 500;
  return res.status(statusCode).json({
    status: "error",
    code: statusCode,
    stack: error.stack,
    message: error.message || "Internal Server Error!",
  });
});

module.exports = app;
