const app = require("./app");

const PORT = process.env.PORT || 3053;
// const PORT = 3053;

const server = app.listen(PORT, () => {
  console.log(`WSV Ecommercial is running port ${PORT}`);
});

process.on("SIGINT", () => {
  server.close(() => {
    console.log("Exit server Express");
  });
});
module.exports = server;
