const mongoose = require("mongoose");
const os = require("os");

const SECONDS = 5000;

// cpunt coneected
const countConnect = () => {
  const numConnect = mongoose.connections.length;
  console.log("Number of connections ", numConnect);
};

// check overload connect
const checkOverload = () => {
  setInterval(() => {
    const numConnection = mongoose.connections.length;
    const numCores = os.cpus().length;
    const memoryUsage = process.memoryUsage().rss;
    // Example maximun number of connections based on number of cores
    const maxConnections = numCores * 5;
    console.log(`Active connections :: ${numConnection}`);
    console.log(`Memory usage:: ${memoryUsage / 1024 / 1024} MB`);

    if (numConnection > maxConnections) {
      console.log(`Connection overload connected`);
    }
  }, SECONDS);
};
module.exports = { countConnect, checkOverload };
