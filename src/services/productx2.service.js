"use strict";

const { BadRequestError } = require("../code/error.response");
const {
  product,
  clothing,
  electronic,
  furniture,
} = require("../models/product.model");

// defined factory class to create product

class ProductFactory {
  /**
   *
   * @param {*} type
   * @param {*} payload
   * type: clothing
   * payload
   */
  static productRegistry = {};

  static registerProductType(type, classRef) {
    ProductFactory.productRegistry[type] = classRef;
  }

  static async createProduct(type, payload) {
    const productClass = ProductFactory.productRegistry[type];
    if (!productClass)
      throw new BadRequestError(`Invalid Product Types ${type}`);
    return new productClass(payload).createProduct();
  }
}

// defined base product class
class Product {
  constructor({
    product_name,
    product_thumb,
    product_description,
    product_price,
    product_quantity,
    product_type,
    product_shop,
    product_attributes,
  }) {
    this.product_name = product_name;
    this.product_thumb = product_thumb;
    this.product_description = product_description;
    this.product_price = product_price;
    this.product_quantity = product_quantity;
    this.product_type = product_type;
    this.product_shop = product_shop;
    this.product_attributes = product_attributes;
  }

  // create product
  async createProduct(product_id) {
    return await product.create({ ...this, _id: product_id });
  }
}

// defined sub-class for different product Types clothing
class Clothings extends Product {
  async createProduct() {
    const newClothing = await clothing.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    if (!newClothing) throw new BadRequestError("create new Clothing error!");

    const newProduct = await super.createProduct(newClothing._id);
    if (!newProduct) throw new BadRequestError("create new Product error");

    return newProduct;
  }
}

class Electronics extends Product {
  async createProduct() {
    const newElectrict = await electronic.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    if (!newElectrict) throw new BadRequestError("Create new Electonic Error");

    const newProduct = await super.createProduct(newElectrict._id);
    if (!newProduct) throw new BadRequestError("Create new Product Error");

    return newProduct;
  }
}

class Furniture extends Product {
  async createProduct() {
    const newFurniture = await furniture.create({
      ...this.product_attributes,
      product_shop: this.product_shop,
    });
    if (!newFurniture) throw new BadRequestError("Create new Furniture Error");

    const newProduct = await super.createProduct(newFurniture._id);
    if (!newProduct) throw new BadRequestError("Create new Product Error");

    return newProduct;
  }
}

// register product
ProductFactory.registerProductType("Clothings", Clothings);
ProductFactory.registerProductType("Electronics", Electronics);
ProductFactory.registerProductType("Furniture", Furniture);

module.exports = ProductFactory;
