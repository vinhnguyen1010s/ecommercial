const shopModel = require("../models/shop.model");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const KeyTokenService = require("./keyToken.service");
const { createTokenPair, verifyJWT } = require("../auth/authUtils");
const { getIntoData } = require("../utils");
const {
  BadRequestError,
  AuthFailureError,
  ForbiddenError,
} = require("../code/error.response");
const { findByEmail } = require("./shop.service");

const rolesShop = {
  SHOP: "SHOP",
  WRITER: "WRITER",
  EDITOR: "EDITOR",
  ADMIN: "ADMIN",
};
class AccessService {
  // static handleRefreshToken = async (refreshToken) => {
  //   // check xem token nay co dang su dung chua
  //   const foundToken = await KeyTokenService.findByRefreshTokensUsed(
  //     refreshToken
  //   );

  //   // neu co
  //   if (foundToken) {
  //     // decode xem token nay co dang dung trong he thong ko
  //     const { userId, email } = await verifyJWT(
  //       refreshToken,
  //       foundToken.privateKey
  //     );
  //     // console.log({ userId, email });

  //     // xoa tac ca token trong keystore
  //     await KeyTokenService.deleteKeyById(userId);
  //     throw new ForbiddenError("Something wrong! please relogin");
  //   }

  //   // Nếu chưa có token
  //   const holderToken = await KeyTokenService.findByRefreshToken(refreshToken);
  //   if (!holderToken) throw new AuthFailureError("Shop not registered 1");

  //   // verify token
  //   const { userId, email } = await verifyJWT(
  //     refreshToken,
  //     holderToken.privateKey
  //   );

  //   // check userId
  //   const foundShop = await findByEmail({ email });

  //   if (!foundShop) throw new AuthFailureError("Shop not registered 2");

  //   // nếu không tìm thấy thì cấp token mới
  //   // create new token
  //   const tokens = await createTokenPair(
  //     { userId, email },
  //     holderToken.privateKey,
  //     holderToken.publicKey
  //   );

  //   // update tokens lại căp token

  //   await holderToken.updateOne({
  //     $set: {
  //       refreshToken: tokens.refreshToken,
  //     },
  //     $addToSet: {
  //       refreshTokensUsed: refreshToken, // da dc su dung de lay tok moi roi
  //     },
  //   });

  //   // trả về cặp token mới
  //   return {
  //     user: { userId, email },
  //     tokens,
  //   };
  // };

  // v2
  static handleRefreshTokenV2 = async ({ keyStore, user, refreshToken }) => {
    const { userId, email } = user;

    if (keyStore.refreshTokensUsed.includes(refreshToken)) {
      await KeyTokenService.deleteKeyById(userId);
      throw new ForbiddenError("Something wrong happened! please relogin");
    }

    if (keyStore.refreshToken !== refreshToken)
      throw new AuthFailureError("Shop not registered");

    // check userId
    const foundShop = await findByEmail({ email });
    if (!foundShop) throw new AuthFailureError("Shop not registered 2");

    // nếu không tìm thấy thì cấp token mới
    // create new token
    const tokens = await createTokenPair(
      { userId, email },
      keyStore.publicKey,
      keyStore.privateKey
    );

    // update tokens lại căp token
    await keyStore.updateOne({
      $set: {
        refreshToken: tokens.refreshToken,
      },
      $addToSet: {
        refreshTokensUsed: refreshToken, // da dc su dung de lay tok moi roi
      },
    });

    // trả về cặp token mới
    return { user, tokens };
  };

  //
  static logout = async (keyStore) => {
    const delKey = await KeyTokenService.removeKeyById(keyStore._id);
    // console.log("delKey:::", delKey);
    return delKey;
  };
  /**
   * 1 - check email in dbs
   * 2 - match password
   * 3 - create AT and RT to save
   * 4 - generate tokens
   * 5 - get data return login
   */
  static login = async ({ email, password, refreshToken = null }) => {
    // 1
    const foundShop = await findByEmail({ email });
    // console.log("foundShop::::", foundShop);
    if (!foundShop) throw new BadRequestError("Shop not registered!");

    // 2
    const match = bcrypt.compare(password, foundShop.password);
    if (!match) throw new AuthFailureError("Authentication error");

    // 3 create privatekey and publickey
    const privateKey = crypto.randomBytes(64).toString("hex");
    const publicKey = crypto.randomBytes(64).toString("hex");

    // 4 generate token
    const { _id: userId } = foundShop;

    const tokens = await createTokenPair(
      { userId, email },
      publicKey,
      privateKey
    );

    await KeyTokenService.createKeyToken({
      refreshToken: tokens.refreshToken,
      userId,
      privateKey,
      publicKey,
    });

    return {
      shop: getIntoData({
        fields: ["_id", "name", "email"],
        object: foundShop,
      }),
      tokens,
    };
  };

  static signUp = async ({ name, email, password }) => {
    // try {

    // step 1 check email exists
    const hoderShop = await shopModel.findOne({ email }).lean();
    if (hoderShop) {
      // return {
      //   code: "xxxx",
      //   message: "Shop already registered",
      // };
      throw new BadRequestError("Error Shop already registered! ");
    }

    const passwordHash = await bcrypt.hash(password, 5);

    const newShop = await shopModel.create({
      name,
      email,
      password: passwordHash,
      roles: [rolesShop.SHOP],
    });

    // step 2 create refrsh token and access token
    if (newShop) {
      // create private key and public key
      // const { privateKey, publicKey } = crypto.generateKeyPairSync("rsa", {
      //   modulusLength: 4096,
      //   publicKeyEncoding: {
      //     type: "pkcs1",
      //     format: "pem",
      //   },
      //   privateKeyEncoding: {
      //     type: "pkcs1",
      //     format: "pem",
      //   },
      // });
      // public key crypto graphy standards

      // chuyển sang dung crypto của nodejs v19
      const privateKey = crypto.randomBytes(64).toString("hex");
      const publicKey = crypto.randomBytes(64).toString("hex");
      // if exists save to keyStore
      // console.log("Key store==", { privateKey, publicKey });

      const keyStore = await KeyTokenService.createKeyToken({
        userId: newShop._id,
        privateKey,
        publicKey,
      });

      if (!keyStore) {
        // throw new BadRequestError("Error Shop already registered! ");
        return {
          code: "xxxxx",
          message: "Public key string error!",
        };
      }

      // const publicKeyObject = crypto.createPublicKey(publicKeyString);
      // console.log("publicKeyObject:::", publicKeyObject);

      // created toke pair
      const tokens = await createTokenPair(
        { userId: newShop._id, email },
        publicKey,
        privateKey
      );
      // console.log(`Created Token success `, tokens);

      return {
        code: 201,
        metadata: {
          // shop: newShop,
          shop: getIntoData({
            fields: ["_id, name, email"],
            object: newShop,
          }),
          tokens,
        },
      };
    }
    // } catch (error) {
    //   return {
    //     code: "xxxx",
    //     message: error.message,
    //     status: "error",
    //   };
    // }
  };
}

module.exports = AccessService;
