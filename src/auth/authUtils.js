"use strict";

const JWT = require("jsonwebtoken");
const { asyncHandle } = require("../helpers/asyncHandle");
const { AuthFailureError, NotFoundError } = require("../code/error.response");
const { findByUserId } = require("../services/keyToken.service");
const { Types } = require("mongoose");

const HEADER = {
  API_KEY: "x-api-key",
  CLIENT_ID: "x-client-id",
  AUTHORIZATION: "authorization",
  REFRESHTOKEN: "x-rtoken-id",
};

const createTokenPair = async (payload, publicKey, privateKey) => {
  try {
    const accessToken = await JWT.sign(payload, publicKey, {
      // algorithm: "RS256",
      expiresIn: "2days",
    });

    const refreshToken = await JWT.sign(payload, privateKey, {
      // algorithm: "RS256",
      expiresIn: "7days",
    });

    JWT.verify(accessToken, publicKey, (err, decode) => {
      if (err) {
        console.log(`Error verify`, err);
      } else {
        console.log(`Decode verify `, decode);
      }
    });
    return { accessToken, refreshToken };
  } catch (error) {
    return error;
  }
};

// const authentication = asyncHandle(async (req, res, next) => {
//   /**
//    * 1 - check userId missing
//    * 2 - get accesstoken
//    * 3 - verify token
//    * 4 - check user in db
//    * 5 - check keyStore with useId
//    * 6 - return next
//    */

//   // 1
//   const userId = req.headers[HEADER.CLIENT_ID];
//   if (!userId) throw new AuthFailureError("Invalid request");

//   // 2
//   const keyStore = await findByUse rId(new Types.ObjectId(userId));
//   if (!keyStore) throw new NotFoundError("Not found keyStore");

//   // 3
//   const accessToken = req.headers[HEADER.AUTHORIZATION];
//   if (!accessToken) throw new AuthFailureError("Invalid keyStore");

//   try {
//     const decodeUser = JWT.verify(accessToken, keyStore.publicKey);

//     if (userId !== decodeUser.userId)
//       throw new AuthFailureError("Invalid userId");

//     req.keyStore = keyStore;
//     return next();
//   } catch (error) {
//     throw error;
//   }
// });

const authenticationV2 = asyncHandle(async (req, res, next) => {
  /**
   * 1 - check userId missing
   * 2 - get accesstoken
   * 3 - verify token
   * 4 - check user in db
   * 5 - check keyStore with useId
   * 6 - return next
   */

  // 1
  const userId = req.headers[HEADER.CLIENT_ID];
  if (!userId) throw new AuthFailureError("Invalid request");

  // 2
  const keyStore = await findByUserId(userId);
  if (!keyStore) throw new NotFoundError("Not found keyStore");

  // 3
  if (req.headers[HEADER.REFRESHTOKEN]) {
    try {
      const refreshToken = req.headers[HEADER.REFRESHTOKEN];
      const decodeUser = JWT.verify(refreshToken, keyStore.privateKey);
      if (userId !== decodeUser.userId)
        throw new AuthFailureError("Invalid userId");
      req.keyStore = keyStore;
      req.user = decodeUser;
      req.refreshToken = refreshToken;
      return next();
    } catch (error) {
      throw error;
    }
  }

  const accessToken = req.headers[HEADER.AUTHORIZATION];
  if (!accessToken) throw new AuthFailureError("Invalid Request");
  console.log("keyStore====", keyStore);
  console.log("accessToken====", accessToken);

  try {
    const decodeUser = JWT.verify(accessToken, keyStore.publicKey);
    console.log("decodeUser====", decodeUser);

    if (userId !== decodeUser.userId)
      throw new AuthFailureError("Invalid userId");
    req.keyStore = keyStore;
    req.user = decodeUser;
    return next();
  } catch (error) {
    throw error;
  }
});

const verifyJWT = async (token, keySecret) => {
  return await JWT.verify(token, keySecret);
};

module.exports = {
  createTokenPair,
  authenticationV2,
  verifyJWT,
};
